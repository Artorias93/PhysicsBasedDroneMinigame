using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneGun : MonoBehaviour
{
    [SerializeField] private Transform gunTransform1;
    [SerializeField] private Transform gunTransform2;
    [SerializeField] private GameObject bullet;
    [SerializeField] private GameObject bulletUp;
    [SerializeField] private float fireRate = 6;

    private float waitTillnextFire = 0;
    private GameObject currBullet;

    private void Start()
    {
        currBullet = bullet;
    }

    void Update()
    {
        Fire();
    }

    void Fire()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            if (waitTillnextFire <= 0)
            {
                Instantiate(currBullet, gunTransform1.position, gunTransform1.rotation);
                Instantiate(currBullet, gunTransform2.position, gunTransform2.rotation);
                waitTillnextFire = 1;
            }
        }

        waitTillnextFire -= fireRate * Time.deltaTime;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Finish")) currBullet = bulletUp;
    }
}
