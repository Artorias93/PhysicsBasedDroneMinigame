using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject diamondList;
    [SerializeField] private GameObject enemies1List;
    [SerializeField] private GameObject enemies2List;
    [SerializeField] private GameObject lastDiamond;
    [SerializeField] private GameObject boss;
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject endPanel;
    [SerializeField] private Items items;
    [SerializeField] private Timer timer;

    private bool isFinished = false;
    private float waitToEnd = 0;

    void Update()
    {
        if (diamondList.transform.childCount == 0 && enemies1List.activeSelf == false) enemies1List.SetActive(true);
        if (enemies1List.transform.childCount == 0 && enemies2List.activeSelf == false) enemies2List.SetActive(true);
        if (enemies2List.transform.childCount == 0 && lastDiamond != null) lastDiamond.SetActive(true);
        
        if (items != null)
        { 
            if (items.getSpawnBoss())
            {
                boss.SetActive(true);
            }
        }

        if (timer.getFinished())
        {
            if (!isFinished)
            {
                waitToEnd = Time.time;
                isFinished = true;
            }

            if (Time.time - waitToEnd > 1.8f)
            {
                endPanel.SetActive(true);
                player.SetActive(false);
            }
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
