using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    public void StartLevel()
    {
        SceneManager.LoadScene("Level1");
    }
    public void ExitGame()
    {
        UnityEditor.EditorApplication.isPlaying = false;
    }

}
