using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour
{
    [SerializeField] private float speed = 0.3f;
    [SerializeField] private float rotspeed = 20;
    [SerializeField] private GameObject gemExplosion;

    private bool goUp;
    private bool spawnBoss = false;

    #region PROPERTIES

    public bool getSpawnBoss()
    {
        return spawnBoss;
    }

    #endregion

    void Start()
    {
        StartCoroutine(SwitchDir());
    }

    void Update()
    {
        if (spawnBoss) Destroy(gameObject);

        if (goUp)
        {
            transform.position = transform.position + new Vector3(0, speed * Time.deltaTime, 0);
        }
        else
        {
            transform.position = transform.position - new Vector3(0, speed * Time.deltaTime, 0);
        }

        transform.Rotate(Vector3.up * (rotspeed * 10 * Time.deltaTime), Space.World);
    }

    IEnumerator SwitchDir()
    {
        while (gameObject.activeSelf)
        {
            yield return new WaitForSeconds(0.75f);
            goUp = !goUp;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameObject gemExpl = Instantiate(gemExplosion, transform.position, transform.rotation);
            GameObject.FindGameObjectWithTag("StageController").GetComponent<StageController>().IncreaseStep();//.SendMessage("IncreaseStep");

            if (gameObject.CompareTag("Finish"))
            {
                Transform[] transformList = gemExpl.GetComponentsInChildren<Transform>();
                
                foreach (Transform i in transformList) i.localScale *= 10;

                spawnBoss = true;

            }

            if (!spawnBoss) Destroy(gameObject);

        }
    }
}
