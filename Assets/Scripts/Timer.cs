using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI timeScore;
    [SerializeField] private TextMeshProUGUI time;
    [SerializeField] private TextMeshProUGUI completationTime;

    private float startTime;
    private bool finished = false;

    #region PROPERTIES

    public bool getFinished()
    {
        return finished;
    }

    #endregion

    void Start()
    {
        startTime = Time.time;
    }

    void Update()
    {
        if (finished) return;

        float t = Time.time - startTime;
        string minutes = ((int)t / 60).ToString("00");
        string seconds = (t % 60).ToString("00.00");

        timeScore.text = minutes + "." + seconds;
    }

    public void Finish()
    {
        finished = true;
        completationTime.text = timeScore.text;
        timeScore.color = Color.cyan;
        time.color = Color.cyan;
    }
}
