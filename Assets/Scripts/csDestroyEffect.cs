﻿using UnityEngine;
using System.Collections;

public class csDestroyEffect : MonoBehaviour {

    [SerializeField] private float destroyTime;

    private void Awake()
    {
        Destroy(gameObject, destroyTime);
    }
}
