using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCam : MonoBehaviour
{
    [SerializeField] private Transform tDrone;
    [SerializeField] private Vector3 behindPos = new Vector3(0, 2, -4);         //Offset rispetto al Drone
    [SerializeField] private Space offsetPositionSpace = Space.Self;
    [SerializeField] private bool lookAt = true;
    private bool bSelf = true;

    void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Q)) bSelf = !bSelf;
        if (bSelf) offsetPositionSpace = Space.Self;
        else offsetPositionSpace = Space.World;

        if (offsetPositionSpace == Space.Self) transform.position = tDrone.TransformPoint(behindPos);
        else transform.position = tDrone.position + behindPos;

        // compute rotation
        if (lookAt) transform.LookAt(tDrone);
        else transform.rotation = tDrone.rotation;
    }
}
