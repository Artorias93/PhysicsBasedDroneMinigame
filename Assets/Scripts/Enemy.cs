using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float enemyHP;
    [SerializeField] private GameObject enemyExplosion;

    private bool toDestroy = false;

    private void Update()
    {
        if (enemyHP <= 0)
        {
            toDestroy = true;
        }
    }

    private void FixedUpdate()
    {
        if (toDestroy)
        {
            GameObject.FindGameObjectWithTag("StageController").GetComponent<StageController>().IncreaseStep();//.SendMessage("IncreaseStep");
            if (gameObject.CompareTag("Finish")) GameObject.FindWithTag("Timer").SendMessage("Finish");
            Instantiate(enemyExplosion, transform.position, transform.rotation);
            Destroy(gameObject);
            toDestroy = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            enemyHP -= collision.gameObject.GetComponent<Bullet>().getBulletDmg();
        }
    }
}
