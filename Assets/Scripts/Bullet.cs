using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float bulletForce;
    [SerializeField] private GameObject bulletExplosion;
    [SerializeField] private float bulletDmg;
    [SerializeField] private float bulletLifetime;

    #region PROPERTIES

    public float getBulletDmg()
    {
        return bulletDmg;
    }

    #endregion

    private void Awake()
    {
        gameObject.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * bulletForce);
        Destroy(gameObject, bulletLifetime);
    }

    private void OnTriggerExit(Collider other)
    {
        gameObject.GetComponent<Collider>().isTrigger = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Bullet"))
        {
            Instantiate(bulletExplosion, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}
