using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StageController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI stageType;
    [SerializeField] private TextMeshProUGUI stageCounter;
    [SerializeField] private TextMeshProUGUI stageInstructions;
    [SerializeField] private GameObject diamondList;
    [SerializeField] private GameObject enemies1List;
    [SerializeField] private GameObject enemies2List;
    [SerializeField] private GameObject panel;

    private int stageId = 0;
    private int stepCounter = 0;
    private int goalStageCounter;
    private float startInfotime;

    void Awake()
    {
        goalStageCounter = diamondList.transform.childCount;
        stageType.text = "Gems\nCollected";
        stageInstructions.text = "Follow the path:\nCollect Gems";
        startInfotime = Time.time;
        stageCounter.text = stepCounter + " / " + goalStageCounter;
    }

    void Update()
    {
        if(Time.time - startInfotime > 5)
        {
            stageInstructions.text = "";
            startInfotime = 0;
        }
    }

    public void IncreaseStep()
    {
        stepCounter++;

        if (stepCounter == goalStageCounter)
        {
            if (stageId == 4)
            {
                stageType.color = Color.cyan;
                stageCounter.color = Color.cyan;
                stageCounter.text = stepCounter + " / " + goalStageCounter;
                return;
            }

            startInfotime = Time.time;
            stepCounter = 0;
            stageId++;

            switch (stageId)
            {
                case 1:
                    panel.SetActive(false);
                    stageType.text = "1� Enemy\nWave";
                    stageInstructions.text = "Follow the path:\nDestroy Grounded Enemies";
                    goalStageCounter = enemies1List.transform.childCount;
                    break;
                case 2:
                    stageType.text = "2� Enemy\nWave";
                    stageInstructions.text = "Follow the path:\nDestroy Flying Enemies";
                    goalStageCounter = enemies2List.transform.childCount;
                    break;
                case 3:
                    stageType.text = "TechGem\nCollected";
                    stageInstructions.text = "Collect the TechGem";
                    goalStageCounter = 1;
                    break;
                case 4:
                    stageType.text = "Boss\nDefeated";
                    stageInstructions.text = "Defeat the Boss:\nWeapons Upgraded!";
                    goalStageCounter = 1;
                    break;
            }
        }

        stageCounter.text = stepCounter + " / " + goalStageCounter;

    }
}
