using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneMovement : MonoBehaviour
{
    [SerializeField] private Rigidbody rbDrone;
    [SerializeField] private AudioSource droneSound;
    private Vector3 velocityToDamp;
    private float absVert;
    private float absHoriz;
    private bool turbo = false;

    [Header("Vertical Movement Settings")]
    [SerializeField] private float idleUpForce = 100;
    [SerializeField] private float upForce = 400;
    [SerializeField] private float downForce = -400;

    [Header("Tilt Settings")]
    [SerializeField] private float tiltSpinForceFwd = 250;
    [SerializeField] private float tiltSpinForceSdw = 250;
    private float tiltVelocityFwd;
    private float tiltVelocitySdw;
    private float tiltAmountFwd = 0;
    private float tiltAmountSdw = 0;

    [Header("Rotation Settings")]
    [SerializeField] private float wantedYRot;
    [SerializeField] private float currentYRot;
    [SerializeField] private float rotAmount = 4f;
    [SerializeField] private float rotYVelocity;

    private void Awake()
    {
        wantedYRot = currentYRot;
    }

    private void FixedUpdate()
    {
        turbo = Input.GetKey(KeyCode.LeftShift);
        absVert = Mathf.Abs(Input.GetAxis("Vertical"));
        absHoriz = Mathf.Abs(Input.GetAxis("Horizontal"));

        VerticalMovement();
        ForwardMovement();
        Rotation();
        ClampingSpeed();
        SidewaysMovement();
        DroneAudio();

        rbDrone.AddRelativeForce(Vector3.up * idleUpForce, ForceMode.Force);                                 //BottomUp Force
        rbDrone.rotation = Quaternion.Euler(new Vector3(tiltAmountFwd, currentYRot, tiltAmountSdw));         //Tilt and Rotate the Drone
    }

    void VerticalMovement() //Handle the Vertical Movement of the Drone
    {
        if (absVert > 0.2f || absHoriz > 0.2f)          //Set BottomUp Force while Tilting
        {
            if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.S))
            {
                rbDrone.velocity = new Vector3(rbDrone.velocity.x, Mathf.Lerp(rbDrone.velocity.y, 0, Time.deltaTime * 5), rbDrone.velocity.z);

                if (turbo) idleUpForce = 800;
                else idleUpForce = 350;
            }
            else if (Input.GetKey(KeyCode.W))
            {
                rbDrone.velocity = new Vector3(rbDrone.velocity.x, Mathf.Lerp(rbDrone.velocity.y, 0, Time.deltaTime * 5), rbDrone.velocity.z);

                if (turbo) idleUpForce = upForce * 3;
                else idleUpForce = upForce * 2;
            }
            else if (Input.GetKey(KeyCode.S))
            {
                rbDrone.velocity = new Vector3(rbDrone.velocity.x, Mathf.Lerp(rbDrone.velocity.y, 0, Time.deltaTime * 5), rbDrone.velocity.z);

                if (turbo) idleUpForce = downForce * 2;
                else idleUpForce = downForce * 1.5f;
            }
        }
        else                                            //Set BottomUp Force while NOT Tilting
        {
            if (Input.GetKey(KeyCode.W))
            {
                if (turbo) idleUpForce = upForce * 1.5f;
                else idleUpForce = upForce;
            }
            else if (Input.GetKey(KeyCode.S))
            {
                if (turbo) idleUpForce = downForce * 2;
                else idleUpForce = downForce;
            }
            else if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.S)) idleUpForce = 98.1f;             //Set BottomUp Force while there's No Movement
        }
    }

    void ForwardMovement()  //Handle the Forward/Backward Movement of the Drone and set the X Rotation (Fwd/Bwd Tilting)
    {
        if (absVert > 0.2f)
        {
            if (turbo) rbDrone.AddRelativeForce(Vector3.forward * Input.GetAxis("Vertical") * tiltSpinForceFwd * 3.25f, ForceMode.Acceleration);
            else rbDrone.AddRelativeForce(Vector3.forward * Input.GetAxis("Vertical") * tiltSpinForceFwd, ForceMode.Acceleration);
            tiltAmountFwd = Mathf.SmoothDamp(tiltAmountFwd, 20 * Input.GetAxis("Vertical"), ref tiltVelocityFwd, 0.1f);          //Set X Rotation (Fwd/Bwd Tilt)
        }
        else
        {
            rbDrone.velocity = new Vector3(Mathf.Lerp(rbDrone.velocity.x, 0, Time.deltaTime), rbDrone.velocity.y, Mathf.Lerp(rbDrone.velocity.z, 0, Time.deltaTime));
            tiltAmountFwd = Mathf.SmoothDamp(tiltAmountFwd, 0, ref tiltVelocityFwd, 0.1f);
        }
    }

    void SidewaysMovement()  //Handle the Sideways Movement of the Drone and set the Z Rotation (Sideways Tilting)
    {
        if (absHoriz > 0.2f)
        {
            if (turbo) rbDrone.AddRelativeForce(Vector3.right * Input.GetAxis("Horizontal") * tiltSpinForceSdw * 3.25f, ForceMode.Acceleration);
            else rbDrone.AddRelativeForce(Vector3.right * Input.GetAxis("Horizontal") * tiltSpinForceSdw, ForceMode.Acceleration);
            tiltAmountSdw = Mathf.SmoothDamp(tiltAmountSdw, -20 * Input.GetAxis("Horizontal"), ref tiltVelocitySdw, 0.1f);          //Set Z Rotation (Sideways Tilt)
        }
        else
        {
            rbDrone.velocity = new Vector3(Mathf.Lerp(rbDrone.velocity.x, 0, Time.deltaTime), rbDrone.velocity.y, Mathf.Lerp(rbDrone.velocity.z, 0, Time.deltaTime));
            tiltAmountSdw = Mathf.SmoothDamp(tiltAmountSdw, 0, ref tiltVelocitySdw, 0.1f);
        }
    }

    void Rotation() //Handle the Y Rotation of the Drone
    {
        if (Input.GetKey(KeyCode.A)) wantedYRot -= rotAmount;
        else if (Input.GetKey(KeyCode.D)) wantedYRot += rotAmount;

        currentYRot = Mathf.SmoothDamp(currentYRot, wantedYRot, ref rotYVelocity, 0.25f);
    }

    void ClampingSpeed()    //Speed Clamping 
    {
        if (absVert > 0.2f && (absHoriz > 0.2f || absHoriz < 0.2f)) rbDrone.velocity = Vector3.ClampMagnitude(rbDrone.velocity, Mathf.Lerp(rbDrone.velocity.magnitude, 10f, Time.deltaTime * 5f));
        if (absVert < 0.2f && absHoriz > 0.2f) rbDrone.velocity = Vector3.ClampMagnitude(rbDrone.velocity, Mathf.Lerp(rbDrone.velocity.magnitude, 5f, Time.deltaTime * 5f));
        if (absVert < 0.2f && absHoriz < 0.2f) rbDrone.velocity = Vector3.SmoothDamp(rbDrone.velocity, Vector3.zero, ref velocityToDamp, 0.95f);
    }

    void DroneAudio()
    {
        droneSound.pitch = 1 + (rbDrone.velocity.magnitude / 50);
    }

}

